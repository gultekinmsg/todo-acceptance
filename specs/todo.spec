# To-do Application Acceptance Tests
* Open todo application
## Displays Added To-do item
* Write "Buy some milk" and click "Add" button
* Must display "Buy some milk"
## Display all Added to-do items
* Add tasks
   |description |
   |------------|
   |first task |
   |second task|
   |third task |
   |fourth task|
   |fifth task |
* Must have
   |description |
   |------------|
   |first task |
   |second task|
   |third task |
   |fourth task|
   |fifth task |
___
* Clear all tasks
