# Acceptance Tests
 - This repository contains acceptance tests for my todo application. 
 - It has one stage in gitlab-ci(`acceptance_test`) that runs all acceptance test
 - This test stage is triggered by backend and frontend pipelines
     - Frontend repository: https://gitlab.com/muhammedgultekin/todo
     - Backend repository: https://gitlab.com/muhammedgultekin/todo-api
 - Staging environment(http://128.199.32.244:8081) is used for acceptance-tests

# Run on your local
To run acceptance tests for todo at local:
1. Make sure your frontend and backend up and running at local
2. Staging url(`128.199.32.244:8081`) has been set by default for acceptance test. You can change this url from: `tests/step_implementation.js:40`
3. Then run from terminal :
> npm install

> npm run test:todo


## Contributors
Muhammed Said Gültekin (gultekinmsg@gmail.com)
