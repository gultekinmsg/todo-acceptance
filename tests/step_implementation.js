/* globals gauge*/
"use strict";
const path = require('path');
const {
    openBrowser,
    write,
    closeBrowser,
    goto,
    screenshot,
    click,
    text,
    evaluate, focus, textBox
} = require('taiko');
const assert = require("assert");
const headless = 'true';

beforeSuite(async () => {
    await openBrowser({
        headless: headless,
        args:["--disable-web-security","--no-sandbox"]
    })
});

afterSuite(async () => {
    await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
    const screenshotFilePath = path.join(process.env['gauge_screenshots_dir'],
        `screenshot-${process.hrtime.bigint()}.png`);

    await screenshot({
        path: screenshotFilePath
    });
    return path.basename(screenshotFilePath);
};

step("Open todo application", async function () {
    await goto("128.199.32.244:8081");
});
step("Clear all tasks", async function () {
    await evaluate(() => localStorage.clear());
});
step("Write <todo_text> and click <button> button", async function (todo_text,button) {
    await focus(textBox({placeholder:'Thing to-do'}))
    await write(todo_text);
    await click(button);
});
step("Must display <todo_text>", async function (todo_text) {
    assert.ok(await text(todo_text).exists());
});
step("Add tasks <table>", async function (table) {
    for (const row of table.rows) {
        await focus(textBox({placeholder:'Thing to-do'}))
        await write(row.cells[0]);
        await click('Add');
    }
});
step("Must have <table>", async function (table) {
    for (const row of table.rows) {
        assert.ok(await text(row.cells[0]).exists());
    }
});
